// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.6.9/firebase-app.js";
import { getAnalytics } from "https://www.gstatic.com/firebasejs/9.6.9/firebase-analytics.js";
import {
    getDatabase,
    ref,
    onValue,
    onChildAdded,
    onChildChanged,
    onChildRemoved,
    query,
    orderByChild,
    limitToFirst,
    limitToLast,
    push
} from "https://www.gstatic.com/firebasejs/9.6.9/firebase-database.js";
import {
    getAuth,
    signInWithPopup,
    GoogleAuthProvider,
    onAuthStateChanged,
    signOut,
    createUserWithEmailAndPassword,
    signInWithEmailAndPassword,
    updatePassword,
    sendPasswordResetEmail
} from "https://www.gstatic.com/firebasejs/9.6.9/firebase-auth.js";


// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyCnxmy53mBDSUHXVp1mXS96xq9uODORo3Q",
    authDomain: "choriweb-742cc.firebaseapp.com",
    databaseURL: "https://choriweb-742cc-default-rtdb.firebaseio.com",
    projectId: "choriweb-742cc",
    storageBucket: "choriweb-742cc.appspot.com",
    messagingSenderId: "887428810068",
    appId: "1:887428810068:web:a97e11fca0f5336988b4c1",
    measurementId: "G-FJBCJLZ2FF"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

// TRABAJO CON DB
const db = getDatabase();
const refTextos = ref(db, "textos/");
const refMensajes = ref(db, "mensajes/");

// QUERIES
const queryMSGbyChild = query(refMensajes, orderByChild("fecha/"));
const queryLimitToFirst = query(refMensajes, limitToFirst(2));


// Elementos

let titulo = document.getElementById("titulo");
let descripcion = document.getElementById("descripcion");
let load = document.getElementById("load");
let chat = document.getElementById("chat");
let user = document.getElementById("user");
let mensaje = document.getElementById("message");
let btnEnviar = document.getElementById("enviar");

let btnLogin = document.getElementById("login");
let chatInputs = document.getElementById("chat__inputs");

let salir = document.getElementById("salir");


let datax = "";

onValue(refTextos, (snap) => {
    // Se ejecuta ante cualquier cambio y siempre muestra todo el nodo
    let data = snap.val();
    console.log("onValue: ", data)
    titulo.innerHTML = data.titulo
    descripcion.innerHTML = data.descripcion
    load.style.display = "none"
    datax = data;
});


// onChildAdded( refTextos, (snap) => {
//   // Se ejecuta al agregar un hijo
//   let data = snap.val();
//   console.log("onChildAdded: ", data)

// });


// onChildChanged( refTextos, (snap) => {
//   // Se ejecuta al cambiar un elemento y solo trae ese elemento
//   let data = snap.val();
//   let key = snap.key;
//   console.log("onChildChanged data: ", data)
//   console.log("onChildChanged key: ", key)

// });


// onChildRemoved( refTextos, (snap) => {
//   // Se ejecuta al cambiar un elemento y solo trae ese elemento
//   let data = snap.val();
//   let key = snap.key;
//   console.log("onChildRemoved data: ", data)
//   console.log("onChildRemoved key: ", key)

// });

// onChildAdded(refMensajes, (snap) => {
//   let data = snap.val();
//   console.log("onChildAdded / mensajes: ", data)

// })
// onValue(queryMensajes, (snap) => {
//     // Query permite pasar parametros de filtrado
//     let data = snap.val();
//     console.log("queryMensajes: ", data)

// });
// FIREBASE
onChildAdded(queryMSGbyChild, (snap) => {
    let data = snap.val();
    let key = snap.key;

    addMSG(data);
})


const addMSG = (msg) => {
    // Se ejecuta en onChildAdded
    let li = document.createElement("li");
    li.classList.add("list__item");
    let txt = document.createTextNode(`${msg.autor} : ${msg.mensaje}`);
    li.appendChild(txt);
    li.setAttribute("id", msg.fecha)
    chat.appendChild(li);
    document.getElementById(msg.fecha).scrollIntoView({ block: "end", behavior: "smooth" })
}



let clicks = document.addEventListener("keyup", (event) => {
    if (event.keyCode === 13) {
        event.preventDefault();
        btnEnviar.click();
    }
})


// OBTENGO MENSAJES
const captureMessagge = () => {
    var date = Date.now()

    if (user.value.trim() != "" && mensaje.value.trim() != "") {
        return {
            autor: user.value,
            mensaje: mensaje.value,
            fecha: date,
        }

    } else {
        alert("Completa los campos")
    }
}



let senMessagge = btnEnviar.addEventListener('click', (e) => {
    push(refMensajes, captureMessagge());
    mensaje.value = "";
})


// LOGIN
const auth = getAuth();

btnLogin.addEventListener('click', () => {
    console.log("LOGIN")
    loguearUsuario();
})

let loguearUsuario = () => {
    auth.languageCode = "es";
    const provider = new GoogleAuthProvider();
    signInWithPopup(auth, provider).then((result) => {
        let logUser = {
            uid: result.user.uid,
            username: result.user.displayName,
            profile_picture: result.user.photoURL,
            email: result.user.email
        }
        user.value = result.user.displayName.split(" ")[0];
        btnLogin.style.display = "none";
        chatInputs.style.display = "block";
        console.log(logUser)
    })

}

// SAVE LOGIN
onAuthStateChanged(auth, (logged) => {
    if (logged) {
        user.value = logged.displayName.split(" ")[0];
        btnLogin.style.display = "none";
        chatInputs.style.display = "block";
    } else {
        null;
    }
})

//LOGOUT
salir.addEventListener("click", () => {
    logout();
})
const logout = () => {
    signOut(auth).then(() => {
        user.value = "";
        btnLogin.style.display = "block";
        chatInputs.style.display = "none";
    })
}
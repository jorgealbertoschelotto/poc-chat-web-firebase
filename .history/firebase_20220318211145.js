// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyCnxmy53mBDSUHXVp1mXS96xq9uODORo3Q",
    authDomain: "choriweb-742cc.firebaseapp.com",
    databaseURL: "https://choriweb-742cc-default-rtdb.firebaseio.com",
    projectId: "choriweb-742cc",
    storageBucket: "choriweb-742cc.appspot.com",
    messagingSenderId: "887428810068",
    appId: "1:887428810068:web:a97e11fca0f5336988b4c1",
    measurementId: "G-FJBCJLZ2FF"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);